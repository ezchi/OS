import java.util.*;

public class Consumer implements Runnable
{
    private BoundedBuffer buffer;
    private String name;

    public Consumer(BoundedBuffer b, String n) {
        buffer = b;
        name = n;
    }

    public void run() {
        while (true) {
            try {
                System.out.println(name + " before retrieve(), size == " + buffer.size());
                String item = (String) buffer.retrieve();
                System.out.println(name + " retrieved: " + item);
            }
            catch(InterruptedException e) { }
      }
   }
}
