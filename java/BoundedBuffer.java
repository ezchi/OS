public interface BoundedBuffer {
    public void insert(Object o) throws InterruptedException;
    public Object retrieve() throws InterruptedException;
    public int size();
}
