# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org
#+OPTIONS: toc:nil

#+TITLE: Just-in-Time Teaching
#+DATE: Computer Structures and Operating Systems 2018
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.

* Motivation
** Initial Problem and Improvement
   - 2016: Classroom response system revealed lack of student understanding
     - Yet, no in-class discussions, leaving me frustrated
       #+ATTR_REVEAL: :frag appear
       - Waste of our time
   #+ATTR_REVEAL: :frag appear
   - After introduction of JiTT: Situation improved

   {{{reveallicense("./figures/org/jitt/JiTT-Java-MX.meta","33vh","figure fragment appear")}}}

** General Improvement in 2017
   {{{reveallicense("./figures/org/jitt/JiTT-Understanding.meta","60vh")}}}

* Just-In-Time Teaching (JiTT)
** Overview
   - Teaching and learning strategy based on web-based study
     assignments (self-learning) and active learner classroom
     - See [[https://jittdl.physics.iupui.edu/jitt/what.html]]
     #+ATTR_REVEAL: :frag (appear)
     - Core idea: *Feedback cycle* among students and instructors
     - Before class
       #+ATTR_REVEAL: :frag (appear)
       - You
	 - *Work* through texts, presentations, videos, exercises, …
	 - Submit solutions to web-based *pre-class assignments*
       - We work through your responses
     - We adjust classroom sessions *just-in-time* based on your
       responses

** Benefits
   #+ATTR_REVEAL: :frag (appear)
   - *Feedback loop*: Your out-of-class preparations affect in-class
     meetings
   - More *structure* for out-of-class learning
     - Content and questions, to be tackled at individual learning pace
   - Better preparation of in-class meetings
     - Identification and correction of
       misconceptions/misunder-standings/incorrect prior beliefs
   - Valuable *shared* time is used more *effectively* for
     student-instructor interactions
     - Traditionally, you figure out what's complicated when you are
       on your own
     - Now, we discuss once you found out what's complicated

** Precondition
   - *Your preparation is essential*
     - Work through assigned pre-class content
     - Solve assignments
   - Starting with preparations for next “lecture”

* JiTT Organization

** JiTT Assignments
   - Upcoming presentations will contain assignments to be submitted by
     you via {{{learnweb}}}
     - With fixed [[JiTT Deadlines][deadlines]]
       - Giving us some time to check solutions ahead of class meetings
   #+ATTR_REVEAL: :frag appear
   - My advice: Solve assignments on your own
     - Of course, you are free to work in groups
       - Still, re-create solutions on your own
     - If you work in groups
       - You *must* submit the group's solution just once
	 - Otherwise, you waste everybody's time
       - Please include names of group members
	 - For an estimate how many students are active

** Bonus Points
   - JiTT assignments *collectively* count as one bonus task
   - 8 or 9 sets of JiTT assignments
     - Fraction of bonus points awarded if you submit reasonable (not
       necessarily correct) solutions for *all* tasks preparing one
       class meeting
       - JiTT assignments will (except for the introductory presentation
         coming up next) include this task,
         which encourages you to *switch your point of view* from
         student to teacher:
	 - {{{understandingquestion}}}
	 - This task *cannot* be solved in groups!

** JiTT Deadlines
   - *Deadlines* for JiTT assignments
     - *Mondays at 10pm*
     - *Thursdays at 10am*
       - No deadline on Thursday if graded test on Wednesday
   - I suggest that you update your timetable after this lecture
     - Add *fixed time slots* for JiTT assignments
       - Last year, students reported *average* workload for
         self-study of 5.2h per week

** Asking for Help
   :PROPERTIES:
   :CUSTOM_ID: help
   :END:
   - Last year, some students struggled on their own for hours,
     getting frustrated
     - Asking search engines
     - Consulting YouTube (sometimes with faulty explanations)
   #+ATTR_REVEAL: :frag appear
   - I suggest to ask (and answer) *earlier* and *elsewhere*
     - (Beware! My online time is limited, my phone’s much more so …)
     - {{{learnweb}}}
     - [[https://about.riot.im/][Riot]] room with
       [[https://en.wikipedia.org/wiki/Etherpad][Etherpad]]
       (room’s address in {{{learnweb}}})
       - Riot is a [[file:Operating-Systems-00-Motivation.org::#free-software][FLOSS]]
         messenger, usable with browser and app
         - Communication under pseudonym, to the point,
           neither greetings nor signatures
         - End-to-end encryption possible, but not enabled in our room
	 - Aims to bridge communication silos, thanks to [[https://matrix.org/blog/home/][Matrix]]
	 - Functionality similar to Slack/Mattermost
       - Etherpad is a FLOSS online editor

#+MACRO: copyrightyears 2017, 2018
#+INCLUDE: license-template.org

# Local Variables:
# indent-tabs-mode: nil
# End:
